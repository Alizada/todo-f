<?php

use App\Http\Controllers\TodoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('todos', TodoController::class);

//| GET|HEAD  | api/todos        | todos.index   | App\Http\Controllers\TodoController@index   | api        |
//|        | POST      | api/todos        | todos.store   | App\Http\Controllers\TodoController@store   | api        |
//|        | GET|HEAD  | api/todos/{todo} | todos.show    | App\Http\Controllers\TodoController@show    | api        |
//|        | PUT|PATCH | api/todos/{todo} | todos.update  | App\Http\Controllers\TodoController@update  | api        |
//|        | DELETE    | api/todos/{todo} | todos.destroy | App\Http\Controllers\TodoController@destroy | api
